//
//  Helpers.swift
//  Classy
//
//  Created by Chris Gilardi on 7/17/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import NSDate_TimeAgo

protocol FirebaseItem {
    var uid : String! { get set }
}

struct UserInfo : FirebaseItem {
    
    var name : String!
    var uid : String!
    var friends : Bool!
    var schoolInfo : SchoolInfo?
    
}

struct SchoolInfo : FirebaseItem {
    
    var name : String!
    var uid : String!
    
}

struct ClassInfo : FirebaseItem {
    
    var callsign : String!
    var name : String!
    var description : String?
    var uid : String!
    var metadata : [String : AnyObject]?
    
}

struct Post : FirebaseItem {
    
    var user : UserInfo!
    var uid : String!
    var body : String!
    var date : NSDate!
    
}

struct Comment : FirebaseItem {
    
    var user : UserInfo!
    var uid : String!
    var body : String!
    var date : NSDate!
}

extension NSDate {

}

extension CGPoint {
    func translatedBy(x: CGFloat, y: CGFloat) -> CGPoint {
        return CGPointMake(self.x + x, self.y + y)
    }
}

extension UIViewController {
    func addModalSwipeDownFunction() {
        let swipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(UserInfoPanelViewController.dismissSelf))
        swipeDownRecognizer.direction = .Down
        self.view.addGestureRecognizer(swipeDownRecognizer)
    }
    
    func dismissSelf() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension UIView {
    func width() -> CGFloat {
        return self.frame.size.width
    }
    
    func height() -> CGFloat {
        return self.frame.size.height
    }
}

extension UIViewController {
    func navbarHeight() -> CGFloat! {
        if self.navigationController != nil {
            return self.navigationController?.navigationBar.frame.height
        } else {
            return 0
        }
    }
}

extension UIViewController {
    func viewWidth() -> CGFloat {
        return view.width()
    }
    
    func viewHeight() -> CGFloat {
        return view.height()
    }
}