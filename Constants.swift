//
//  Constants.swift
//  Classy
//
//  Created by Chris Gilardi on 7/9/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import UIKit

//MARK: IF DEBUGGING
let debug = false

enum SchoolType {
    case K8
    case HighSchool
    case University
}

class Colors {
    static let darkPrimary = UIColor(hexString: "#e74c3c")!
    static let lightPrimary = UIColor(hexString: "#f85d4d")!
    
    static let darkGray = UIColor(hexString: "#34495e")!
    static let lightGray = UIColor(hexString: "#455a6f")!
    
    static let white = UIColor.whiteColor()
    static let offWhite = UIColor(hexString: "#EEEEEE")!
    static let black = UIColor.blackColor()
    
    static let buttonOrange = UIColor(hexString: "#FF5722")!
    
    static let blue = UIColor(hexString: "#0865C1")!
    
    
}

let kSTMidButton = "kSTMidButtonID"
let kSThsButton = "hsButtonID"
let kSTuniButton = "uniButtonID"

let kSchoolChooserButton = "openChooserModalID"



let kPostCellIdentifier = "postCellID"
let kUserActionCellIdentifier = "userActionCellID"
let kCommentCellIdentifier = "commentCellID"

let kLorem = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius adipisci, sed libero. Iste asperiores suscipit, consequatur debitis animi impedit numquam facilis iusto porro labore dolorem, maxime magni incidunt. Delectus, est! \n Totam at eius excepturi deleniti sed, error repellat itaque omnis maiores tempora ratione dolor velit minus porro aspernatur repudiandae labore quas adipisci esse, nulla tempore voluptatibus cupiditate. Ab provident, atque. \n Possimus deserunt nisi perferendis, consequuntur odio et aperiam, est, dicta dolor itaque sunt laborum, magni qui optio illum dolore laudantium similique harum. Eveniet quis, libero eligendi delectus repellendus repudiandae ipsum? \n Vel nam odio dolorem, voluptas sequi minus quo tempore, animi est quia earum maxime. Reiciendis quae repellat, modi non, veniam natus soluta at optio vitae in excepturi minima eveniet dolor."