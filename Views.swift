//
//  Views.swift
//  Classy
//
//  Created by Chris Gilardi on 7/10/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import UIKit
import pop
import Async
import NVActivityIndicatorView

class FloatingInfoBar : UIView {
    
    let cornerRadius : CGFloat = 15
    var titleLabel : UILabel!
    var showsShadow : Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clearColor()
        
        let background = UIVisualEffectView(effect: UIBlurEffect(style: .ExtraLight))
        background.frame = CGRectMake(0, 0, frame.width, frame.height)
        background.layer.cornerRadius = self.cornerRadius
        background.clipsToBounds = true
        self.addSubview(background)
        
        let upper = UIVisualEffectView(effect: UIBlurEffect(style: .ExtraLight))
        upper.frame = CGRectMake(0, 0, background.frame.size.width, 30)
        background.addSubview(upper)
        
        titleLabel = UILabel(frame: CGRectMake(0,0,upper.frame.size.width/2, 30))
        titleLabel.center = CGPointMake(upper.frame.size.width/2, upper.frame.size.height/2)
        titleLabel.textColor = Colors.darkGray
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.font = UIFont.systemFontOfSize(18)
        upper.addSubview(titleLabel)
        
        setupLayer()
    }
    
    func setupLayer() {
        self.layer.cornerRadius = self.cornerRadius
        //self.clipsToBounds = true
        if self.showsShadow {
            self.layer.shadowColor = Colors.black.CGColor
            self.layer.shadowRadius = 10
            self.layer.shadowOpacity = 0.75
        }
    }
    
    func setTitle(text : String?) {
        self.titleLabel.text = text
    }
    
    func setMessage() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
}

class RadioButton : UIView {
    
    var messageLabel : UILabel!
    var radioButton : UIButton!
    var delegate : RadioButtonDelegate?
    var identifier : String!
    var selectionColor : UIColor? = Colors.buttonOrange
    var deselectedColor : UIColor! = Colors.white {
        didSet {
            self.reset(animated: false)
        }
    }
    var tapped = false
    
    var callback : (() -> Void)?
    
    init(frame: CGRect = CGRectZero, identifier : String!) {
        super.init(frame: frame)
        
        self.identifier = identifier
        self.backgroundColor = deselectedColor
        
        messageLabel = UILabel(frame: CGRectMake(0,0,self.frame.size.width,self.frame.size.height))
        messageLabel.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2)
        messageLabel.textAlignment = .Center
        messageLabel.lineBreakMode = .ByWordWrapping
        self.addSubview(messageLabel)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(RadioButton.onTap))
        self.layer.cornerRadius = 10
        self.addGestureRecognizer(tapRecognizer)
    }
    
    func onTap() {
        delegate?.radioButtonWasTapped(identifier: identifier, selected: self.tapped)
        if let cb = self.callback {
            cb()
        }
        if tapped {
            UIView.animateWithDuration(0.5, animations: {() -> Void in
                self.backgroundColor = self.deselectedColor
                self.messageLabel.textColor = Colors.black
            })
            tapped = false
        } else {
            UIView.animateWithDuration(0.5, animations: {() -> Void in
                self.backgroundColor = self.selectionColor
                self.messageLabel.textColor = Colors.white
            })
            tapped = true
        }

    }
    
    func reset(animated a: Bool) {
        if a {
            UIView.animateWithDuration(0.5, animations: {() -> Void in
                self.backgroundColor = self.deselectedColor
                self.messageLabel.textColor = Colors.black
                self.tapped = false
            })
        } else {
            self.backgroundColor = self.deselectedColor
            self.messageLabel.textColor = Colors.black
            self.tapped = false
        }
    }
    
    func setLabel(title: String) -> RadioButton {
        self.messageLabel.text = title
        return self
    }
    
    func onSelect(function cb : () -> Void) -> RadioButton {
        self.callback = cb
        return self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class RadioButtonSet : UIView, RadioButtonDelegate {
    
    //MARK: Properties
    var identifier : String!
    
    //MARK: Views
    var backgroundView : FloatingInfoBar!
    var buttons : [RadioButton!]! = []
    var buttonArea : UIView!
    
    //MARK: Button Properties
    private var buttonColor: UIColor! = Colors.white
    private var selectedColor: UIColor! = Colors.buttonOrange
    var desiredHeight : CGFloat!
    var previousButtonPoint : CGPoint?
    
    //MARK: Delegate
    var delegate : RadioButtonSetDelegate?
    
    var collapsesOnSelection = true
    
    convenience init(frame: CGRect, title: String?, identifier: String!, buttonColor: UIColor?, selectedColor: UIColor?) {
        self.init(frame: frame)
        desiredHeight = self.height()
        backgroundView = FloatingInfoBar(frame: CGRectMake(0,0,self.width(), self.height()))
        backgroundView.setTitle(title)
        if let bC = buttonColor {
            self.buttonColor = bC
        }
        
        if let sC = selectedColor {
            self.selectedColor = sC
        }
        
        self.addSubview(backgroundView)
        
        buttonArea = UIView(frame: CGRectMake(0,30,frame.size.width, frame.size.height - 30))
        
        self.backgroundView.addSubview(buttonArea)
        
        if debug {
            buttonArea.backgroundColor = .redColor()
            self.backgroundColor = .greenColor()
        }
    }
    
    func addButtons(b : RadioButton...) {
        for button in b {
            self.addButton(button)
        }
    }
    
    func addButton(b : RadioButton!) {
        
        //Add to view
        b.frame = CGRectMake(0, 0, self.width() - 40, (self.height() / (CGFloat(self.buttons.count) + 1)) - 40)
        b.center = self.center.translatedBy(0, y: 50)
        b.selectionColor = self.selectedColor
        b.deselectedColor = self.buttonColor
        b.delegate = self
        self.buttonArea.addSubview(b)
        
        //Add to array of buttons
        self.buttons.append(b)
        
        self.layoutSubviews()
    }
    
    func addButton( button b : RadioButton , title t : String! ) {
        b.setLabel(t)
        self.addButton(b)
    }
    
    func radioButtonWasTapped(identifier id: String!, selected: Bool!) {
        var butt : RadioButton!
        //TODO: Figure out which button was tapped
        for button in buttons {
            if button.identifier != id {
                if collapsesOnSelection {
                    if selected == true {
                        Async.main(after: 1.2, block: {
                            UIView.animateWithDuration(0.3, animations: {
                                button.alpha = 1.0
                            })
                            button.userInteractionEnabled = true
                        })
                    } else {
                        UIView.animateWithDuration(0.3, animations: {
                            button.alpha = 0.0
                        })
                        button.userInteractionEnabled = false
                    }
                }
            } else {
                butt = button
            }
        }
        
        if selected == true {
            let expandimation = POPBasicAnimation(propertyNamed: kPOPLayerSize)
            expandimation.toValue = NSValue(CGSize: CGSizeMake(self.backgroundView.layer.frame.size.width, desiredHeight))
            
            let moveButtBack = POPBasicAnimation(propertyNamed: kPOPViewCenter)
            moveButtBack.toValue = NSValue(CGPoint: previousButtonPoint!)
            
            Async.main(after: 0.5, block: {
                self.delegate?.radioButtonSetDidExpand(self)
                Async.main(after: 0.5, block: {
                    if self.collapsesOnSelection {
                        self.backgroundView.layer.sublayers![0].pop_addAnimation(expandimation, forKey: "expand")
                        butt.pop_addAnimation(moveButtBack, forKey: "moveButtBack")
                    }
                })
            })
        } else {
            let shrinkAnimation = POPBasicAnimation(propertyNamed: kPOPLayerSize)
            shrinkAnimation.toValue = NSValue(CGSize: CGSizeMake(self.backgroundView.layer.frame.size.width, butt.height() + 60))
            
            previousButtonPoint = butt.center
            
            let moveButtToCenter = POPBasicAnimation(propertyNamed: kPOPViewCenter)
            moveButtToCenter.toValue = NSValue(CGPoint: CGPointMake(self.buttonArea.width()/2, self.buttonArea.height()/2))
            
            
            if self.collapsesOnSelection {
                Async.main(after: 0.5, block: {
                    self.backgroundView.layer.sublayers![0].pop_addAnimation(shrinkAnimation, forKey: "shrink")
                    butt.pop_addAnimation(moveButtToCenter, forKey: "moveButtToCenter")
                    Async.main(after: 0.5, block: {
                        self.delegate?.radioButtonSetDidCollapse(selectedID: butt.identifier, radioButtonSet: self)
                    })
                })
            } else {
                self.delegate?.radioButtonSetDidCollapse(selectedID: butt.identifier, radioButtonSet: self)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let numButtons = buttons.count
        
        if numButtons == 1 {
            buttons[0].frame = CGRectMake(0, 0, self.buttonArea.width() - 40, self.buttonArea.height() - 40)
            buttons[0].center = CGPointMake(self.buttonArea.width()/2, self.buttonArea.height()/2)
        } else {
            for i in 0...(numButtons - 1) {
                let heightNeeded = (self.buttonArea.height() / CGFloat(numButtons)) - 30
                let yPos = (20 + heightNeeded/2) + (CGFloat(i)*(heightNeeded + 20))
                
                buttons[i].frame = CGRectMake(0, 0, self.width() - 40, heightNeeded)
                buttons[i].center = CGPointMake(self.buttonArea.width()/2, CGFloat(yPos))
                buttons[i].messageLabel.frame.size = buttons[i].frame.size
            }
        }
    }
    
    //Required
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class LoadingView : UIView {
    
    var loadingView : NVActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let background = UIVisualEffectView(effect: UIBlurEffect(style: .ExtraLight))
        background.frame = self.frame
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        
        self.addSubview(background)
        
        loadingView = NVActivityIndicatorView(frame: CGRectMake(0, 0, self.width()/2, self.width()/2), type: .LineScaleParty, color: Colors.buttonOrange, padding: nil)
        loadingView.center = background.center
        
        background.addSubview(loadingView)
        
    }
    
    func stopAnimating() {
        loadingView.stopAnimation()
    }
    
    func startAnimating() {
        loadingView.startAnimation()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

protocol RadioButtonDelegate {
    
    func radioButtonWasTapped(identifier id : String!, selected: Bool!)
    
}

protocol RadioButtonSetDelegate {
    
    func radioButtonSetDidCollapse(selectedID identifier : String, radioButtonSet : RadioButtonSet)
    func radioButtonSetDidExpand(radioButtonSet : RadioButtonSet)
    
}



