//
//  ViewController.swift
//  Classy
//
//  Created by Chris Gilardi on 7/8/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import UIKit

class NavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = UIColor(hexString: "#455a6f")!
        self.navigationBar.tintColor = .whiteColor()
        
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor() /*UIColor(hexString: "#e74c3c")!*/]
        
        self.navigationBar.layer.shadowOpacity = 0.5
        self.navigationBar.layer.shadowColor = UIColor.blackColor().CGColor
        self.navigationBar.layer.shadowRadius = 5
        
        removeShadowFromNavbar()

    }
    
    func removeShadowFromNavbar() {
        for parent in self.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

