//
//  PostViewController.swift
//  Classy
//
//  Created by Chris Gilardi on 7/18/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import UIKit
import Async

class PostViewController : UITableViewController {
    
    var post : Post!
    var navController : UINavigationController!
    
    convenience init(post p: Post!, navController nC : UINavigationController!) {
        self.init()
        self.post = p
        self.navController = nC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Comments"
        
        self.tableView.backgroundColor = Colors.offWhite
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(PostViewController.reloadComments(_:)), forControlEvents: .ValueChanged)
        refreshControl.tintColor = Colors.buttonOrange
        self.tableView.addSubview(refreshControl)
        
        setupNavbar()
    }
    
    func setupNavbar() {
        let shareButton = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: #selector(PostViewController.openSharePane))
        self.navigationItem.rightBarButtonItem = shareButton
    }
    
    func openSharePane() {
        
        let activityView = UIActivityViewController(activityItems: ["https://google.com/"], applicationActivities: nil)
        
        self.presentViewController(activityView, animated: true, completion: nil)
    }
    
    func reloadComments(sender : UIRefreshControl) {
        //TODO: Have this reload the comments from Firebase
        self.tableView.reloadData()
        Async.main(after: 0.5, block: {
            sender.endRefreshing()
        })
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 && indexPath.section == 0 {
            return 300
        }
        return 100
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell?
        if indexPath.row == 0 && indexPath.section == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier(kPostCellIdentifier)
            
            if cell == nil {
                cell = PostCell(post: self.post, navController: self.navController)
            }
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier(kCommentCellIdentifier)
            
            if cell == nil {
                cell = CommentCell(style: .Default, reuseIdentifier: kCommentCellIdentifier)
            }
        }
        
        if indexPath.row == 0 && indexPath.section == 0 {
            (cell as! PostCell).opensPost = false
            cell?.selectionStyle = .None
        }
        
        return cell!
    }
    
}

//TODO: Implement Comment Cell
class CommentCell : UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.textLabel?.text = "Comment..."
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
}