//
//  AddClassViewController.swift
//  Classy
//
//  Created by Chris Gilardi on 7/9/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import UIKit
import pop
import Async

class AddClassNavController : UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavbar()
        addModalSwipeDownFunction()
    }
    
    func configureNavbar() {
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationBar.backgroundColor = Colors.darkGray
        self.navigationBar.shadowImage = UIImage()
        
        let statusBar = UIView(frame: CGRectMake(0,0,self.view.width(), 20))
        statusBar.backgroundColor = self.navigationBar.backgroundColor
        self.view.addSubview(statusBar)
        
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : Colors.white]
        self.navigationBar.tintColor = Colors.white
        
    }
    
}

///Shows intro on classes -- decides which controller to send user to.
class AddClassMainViewController : UIViewController, RadioButtonSetDelegate {
    
    //MARK: Buttons
    var k12Button : RadioButton!
    var hsButton : RadioButton!
    var uniButton : RadioButton!
    var openChooserModalButton : RadioButton!
    
    //MARK: Views
    var scrollView : UIScrollView!
    var rbSet1 : RadioButtonSet!
    
    var schools : [String] = ["SDSU", "Cal Poly SLO", "UCSB", "UC Berkeley"]
    
    private var schoolTypeChosen = false
    private var schoolChosen = false
    private var classChosen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.lightGray
        self.title = "Add a Class"
        
        self.scrollView = UIScrollView(frame: CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height * 2))
        self.scrollView.backgroundColor = .clearColor()
        self.scrollView.scrollEnabled = true
        self.view.addSubview(scrollView)
        
        let closeButton = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(AddClassNavController.dismissSelf))
        self.navigationItem.leftBarButtonItem = closeButton
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(AddClassNavController.dismissSelf))
        doneButton.enabled = false
        self.navigationItem.rightBarButtonItem = doneButton

        self.setupSchoolTypePane()
        self.setupSchoolChooserPanel()
    }
    
    func setupSchoolTypePane() {
        rbSet1 = RadioButtonSet(frame: CGRectMake(0, 60, self.view.width() - 40, 300), title: "Choose Your Grade Level", identifier: "schoolTypeChooser", buttonColor: Colors.white, selectedColor: Colors.buttonOrange)
        rbSet1.center = self.view.center
        rbSet1.delegate = self
        
        let k8Button = RadioButton(identifier: kSTMidButton).setLabel("Middle School (6-8)")
        let hsButton = RadioButton(identifier: kSThsButton).setLabel("High School")
        let uniButton = RadioButton(identifier: kSTuniButton).setLabel("University")
        
        rbSet1.addButtons(k8Button, hsButton, uniButton)
        
        self.view.addSubview(rbSet1)
    }
    
    func setupSchoolChooserPanel() {
        
    }
    
    func radioButtonSetDidCollapse(selectedID identifier: String, radioButtonSet : RadioButtonSet) {
        //Decide what to do based on a button's Identifier
        print("\(identifier) was chosen")
        let moveUpAnim = POPBasicAnimation(propertyNamed: kPOPViewCenter)
        moveUpAnim.toValue = NSValue(CGPoint: CGPointMake(self.view.center.x, self.view.frame.size.height/4))
        radioButtonSet.pop_addAnimation(moveUpAnim, forKey: "moveUp1")
        
        self.navigationItem.rightBarButtonItem?.enabled = true
        
    }
    
    func radioButtonSetDidExpand(radioButtonSet : RadioButtonSet) {
        let moveDownAnim = POPBasicAnimation(propertyNamed: kPOPViewCenter)
        moveDownAnim.toValue = NSValue(CGPoint: self.view.center)
        radioButtonSet.pop_addAnimation(moveDownAnim, forKey: "moveDown1")
        
        self.navigationItem.rightBarButtonItem?.enabled = false
    }
}


class ACSchoolPickerController : UITableViewController {
 //TODO: Remember School
    private var schoolType : SchoolType!
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ACClassPickerController : UIViewController {
    //TODO: In future, skip straight to here. Save School info to account.
    private var schoolInfo : [String : String]!
    
    convenience init(schoolInfo si : [String : String]!) {
        self.init()
        self.schoolInfo = si
    }
    
}