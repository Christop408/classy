//
//  UserViewController.swift
//  Classy
//
//  Created by Chris Gilardi on 7/17/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import UIKit
import Async
import pop

class UserViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var userInfo : UserInfo?
    var actionTableView : UITableView!
    
    convenience init(userInfo: UserInfo?) {
        self.init()
        self.userInfo = userInfo
        self.view.backgroundColor = Colors.offWhite
        
        self.setupNavbar()
        self.designBackgroundView()
        
        self.title = "Info"
        
        let profilePicView = UIImageView(image: UIImage(named: "cgilardi"))
        profilePicView.frame = CGRectMake(0, 0, 150, 150)
        profilePicView.center = CGPointMake(self.view.width()/2, 100)
        profilePicView.layer.cornerRadius = profilePicView.width()/2
        profilePicView.clipsToBounds = true
        profilePicView.layer.borderColor = Colors.offWhite.CGColor
        profilePicView.layer.borderWidth = 4
        
        self.view.addSubview(profilePicView)
        
        let schoolImageView = UIImageView(image: UIImage(named: "sdsu"))
        schoolImageView.frame = CGRectMake(0, 0, 40, 40)
        schoolImageView.center = profilePicView.center.translatedBy(45, y: 45)
        schoolImageView.layer.backgroundColor = Colors.white.CGColor
        schoolImageView.layer.borderWidth = 2
        schoolImageView.layer.borderColor = Colors.offWhite.CGColor
        schoolImageView.clipsToBounds = true
        schoolImageView.layer.cornerRadius = schoolImageView.width()/2
        schoolImageView.contentMode = .ScaleAspectFit
        schoolImageView.layer.shadowColor = UIColor.blackColor().CGColor
        schoolImageView.layer.shadowOpacity = 0.5
        schoolImageView.layer.shadowRadius = 7
        
        self.view.addSubview(schoolImageView)
        
        let userNameLabel = UILabel(frame: CGRectMake(0,0,self.view.width(), 20))
        userNameLabel.text = userInfo?.name
        userNameLabel.textColor = Colors.white
        userNameLabel.center = CGPointMake(self.view.width()/2, 190)
        userNameLabel.textAlignment = .Center
        
        self.view.addSubview(userNameLabel)
        
        self.actionTableView = UITableView(frame: CGRectMake(0, 210, self.view.width(), self.view.height()-210), style: .Plain)
        self.actionTableView.delegate = self
        self.actionTableView.dataSource = self
        self.actionTableView.separatorStyle = .None
        self.actionTableView.showsVerticalScrollIndicator = false
        self.view.addSubview(actionTableView)
    }
    
    func setupNavbar() {
        if userInfo?.friends == true {
            let settingsItem = UIBarButtonItem(image: UIImage(named: "settingsButton"), style: .Plain, target: self, action: #selector(UserViewController.showFriendSettings))
            self.navigationItem.rightBarButtonItem = settingsItem
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    func showFriendSettings() {
        let friendSettings = UIAlertController(title: "Friend Settings", message: nil, preferredStyle: .ActionSheet)
        
        friendSettings.addAction(UIAlertAction(title: "Remove from Friends", style: .Destructive, handler: {action -> Void in
            print("Deleting Friend: \(self.userInfo?.name)")
            (self.actionTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as! AddFriendCell).reset(animated: true)
        }))
        
        friendSettings.addAction(UIAlertAction(title: "Close", style: .Cancel, handler: {action -> Void in
            friendSettings.dismissSelf()
        }))
        self.presentViewController(friendSettings, animated: true, completion: nil)
    }
    
    func handleFriendStatusChange() {
        //TODO: Implement Correctly.
        //FIXME: Does not actually use any sort of online service yet.
        if self.userInfo?.friends == true {
            print(self.userInfo?.friends)
            self.userInfo?.friends = false
        } else if self.userInfo?.friends == false {
            print(self.userInfo?.friends)
            self.userInfo?.friends = true
        }
        self.setupNavbar()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            break
        case 1:
            handleFriendStatusChange()
            (tableView.cellForRowAtIndexPath(indexPath) as! AddFriendCell).handleTap()
            break
        case 2:
            break
        default:
            break
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(kUserActionCellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: kUserActionCellIdentifier)
        }
        
        let cellHeight = self.tableView(tableView, heightForRowAtIndexPath: indexPath)
        let iconSize = cellHeight - 30
        
        switch indexPath.row {
        case 0:
            cell?.backgroundColor = .clearColor()
            let recentImage = UIImageView(image: UIImage(named: "new"))
            recentImage.frame = CGRectMake(0, 0, iconSize, iconSize)
            recentImage.center = CGPointMake(20 + recentImage.width()/2, cellHeight/2)
            cell?.addSubview(recentImage)
            
            let label = UILabel(frame: CGRectMake(recentImage.frame.origin.x + recentImage.width() + 30, cellHeight/2 - 15, 150, 30))
            label.text = "Recent Posts"
            
            cell?.addSubview(label)
            
            break
        case 1:
            cell = AddFriendCell(style: .Default, reuseIdentifier: kUserActionCellIdentifier, requested: false)
            break
        case 2:
            cell?.backgroundColor = .clearColor()
            let chatImage = UIImageView(image: UIImage(named: "chat-filled"))
            chatImage.frame = CGRectMake(0, 0, iconSize, iconSize)
            chatImage.center = CGPointMake(20 + chatImage.width()/2, cellHeight/2)
            cell?.addSubview(chatImage)
            
            let label = UILabel(frame: CGRectMake(chatImage.frame.origin.x + chatImage.width() + 30, cellHeight/2 - 15, 150, 30))
            label.text = "Send a Message"
            
            cell?.addSubview(label)
            
            break
        default:
            break
        }
        
        return cell!
    }
    
    func designBackgroundView() {
        let upperHeader = CAShapeLayer()
        upperHeader.path = CGPathCreateWithRect(CGRectMake(0, 0, self.view.width(), 96), nil)
        upperHeader.fillColor = Colors.buttonOrange.CGColor
        self.view.layer.addSublayer(upperHeader)
        
        let lowerHeader = CAShapeLayer()
        lowerHeader.path = CGPathCreateWithRect(CGRectMake(0, 104, self.view.width(), 106), nil)
        lowerHeader.fillColor = Colors.lightGray.CGColor
        self.view.layer.addSublayer(lowerHeader)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

class AddFriendCell : UITableViewCell {
    
    var addFriendImage : UIImageView!
    var label : UILabel!
    
    //MARK: For use in changing the text
    var undoTextLabel : UILabel!
    var newTextLabel : UILabel!
    
    var userIsRequested : Bool = false
    
    convenience init(style: UITableViewCellStyle, reuseIdentifier: String?, requested : Bool) {
        self.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = Colors.buttonOrange
        
        addFriendImage = UIImageView(image: UIImage(named: "add-friend"))
        addFriendImage.frame = CGRectMake(0, 0, 100-30, 100-30)
        addFriendImage.center = CGPointMake(20 + addFriendImage.width()/2, 100/2)
        self.addSubview(addFriendImage)
        
        label = UILabel(frame: CGRectMake(addFriendImage.frame.origin.x + addFriendImage.width() + 30, (100)/2 - 15, 150, 30))
        label.text = "Add to Friends"
        label.textColor = Colors.white
        
        self.addSubview(label)
        
        newTextLabel = UILabel(frame: label.frame)
        newTextLabel.text = "Friend Requested"
        newTextLabel.alpha = 0
        newTextLabel.textColor = Colors.white
        self.addSubview(newTextLabel)
        
        undoTextLabel = UILabel(frame: CGRectMake(addFriendImage.frame.origin.x + addFriendImage.width() + 30, 100/2, 100, 20))
        undoTextLabel.text = "Tap again to undo"
        undoTextLabel.adjustsFontSizeToFitWidth = true
        undoTextLabel.textColor = Colors.offWhite
        undoTextLabel.alpha = 0
        self.addSubview(undoTextLabel)
        
        if requested {
            changeToRequested(animated: false)
            userIsRequested = requested
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func handleTap() {
        if !userIsRequested {
            changeToRequested(animated: true)
        } else {
            reset(animated: true)
        }
    }
    
    func reset(animated a: Bool) {
        resetText(animated: a)
        resetColor(animated: a)
        userIsRequested = false
    }
    
    func changeToRequested(animated a: Bool) {
        changeText(animated: a)
        turnGray(animated: a)
        userIsRequested = true
    }
    
    func changeText(animated animated : Bool) {
        if animated {
            UIView.animateWithDuration(0.5, animations: {
                self.label.alpha = 0
            })
            
            Async.main(after: 0.5, block: {
                UIView.animateWithDuration(0.5, animations: {
                    self.newTextLabel.alpha = 1.0
                })
                Async.main(after: 0.4, block: {
                    let moveUp = POPBasicAnimation(propertyNamed: kPOPViewCenter)
                    moveUp.toValue = NSValue(CGPoint: self.newTextLabel.center.translatedBy(0, y: -8))
                    self.newTextLabel.pop_addAnimation(moveUp, forKey: "moveUp")
                    
                    Async.main(after: 0.3, block: {
                        UIView.animateWithDuration(0.2, animations: {
                            self.undoTextLabel.alpha = 1.0
                        })
                    })
                })
            })
        } else {
            self.label.alpha = 0
            self.newTextLabel.alpha = 1.0
            newTextLabel.center = self.newTextLabel.center.translatedBy(0, y: -8)
            self.undoTextLabel.alpha = 1.0
        }
        
    }
    
    func turnGray(animated animated : Bool) {
        if animated {
            UIView.animateWithDuration(0.5, animations: {
                self.backgroundColor = Colors.lightGray
            })
        } else {
            self.backgroundColor = Colors.lightGray
        }
    }
    
    func resetText(animated animated : Bool) {
        
        if animated {
            UIView.animateWithDuration(0.2, animations: {
                self.undoTextLabel.alpha = 0.0
            })
            
            Async.main(after: 0.3, block: {
                let moveDown = POPBasicAnimation(propertyNamed: kPOPViewCenter)
                moveDown.toValue = NSValue(CGPoint: self.newTextLabel.center.translatedBy(0, y: 8))
                self.newTextLabel.pop_addAnimation(moveDown, forKey: "moveDown")
                
                Async.main(after: 0.4, block: {
                    UIView.animateWithDuration(0.5, animations: {
                        self.newTextLabel.alpha = 0
                    })
                    Async.main(after: 0.5, block: {
                        UIView.animateWithDuration(0.5, animations: {
                            self.label.alpha = 1.0
                        })
                    })
                })
            })
        } else {
            self.undoTextLabel.alpha = 0.0
            self.newTextLabel.center = self.newTextLabel.center.translatedBy(0, y: 8)
            self.newTextLabel.alpha = 0
            self.label.alpha = 1.0
        }
        
    }
    
    func resetColor(animated animated : Bool) {
        if animated {
            UIView.animateWithDuration(0.5, animations: {
                self.backgroundColor = Colors.buttonOrange
            })
        } else {
            self.backgroundColor = Colors.buttonOrange
        }
    }
    
}