//
//  UserInfoPanelViewController.swift
//  Classy
//
//  Created by Chris Gilardi on 7/9/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import UIKit
import SwiftHEXColors

class UserInfoPanelViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var optionTableView : UITableView!
    var navbar : UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hexString: "#EEEEEE")
    }
    
    override func viewWillAppear(animated: Bool) {
        configureNavbar()
        optionTableView = UITableView(frame: CGRectMake(0, navbar.frame.height, self.view.frame.size.width, self.view.frame.size.height - navbar.frame.height), style: .Grouped)
        self.view.addSubview(optionTableView)
        self.optionTableView.delegate = self
        self.optionTableView.dataSource = self
    }
    
    func configureNavbar() {
        navbar = UINavigationBar(frame: CGRectMake(0,0,self.view.frame.width, self.view.frame.height/3))
        navbar.barTintColor = .whiteColor()
        self.view.addSubview(navbar)
        navbar.tintColor = .whiteColor()
        
        let item = UINavigationItem(title: "Chris Gilardi")

        navbar.barStyle = .Default
    
        item.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Stop, target: self, action: #selector(UserInfoPanelViewController.dismissSelf))
        navbar.items = [item]
        
        navbar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        navbar.backgroundColor = UIColor(hexString: "#455a6f")!
        navbar.shadowImage = UIImage()
        navbar.layer.shadowRadius = 5
        navbar.layer.shadowOpacity = 0.5
        navbar.layer.shadowColor = UIColor.blackColor().CGColor
        navbar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        let userImage = UIImage(named: "cgilardi")
        let userImageView = UIImageView(image: userImage)
        userImageView.frame = CGRectMake(0, 0, self.view.frame.width/3, self.view.frame.width/3)
        userImageView.center = CGPointMake(navbar.frame.width/2, navbar.frame.height/2)
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.clipsToBounds = true
        userImageView.layer.borderWidth = 4
        userImageView.layer.borderColor = UIColor.whiteColor().CGColor
        
        
        navbar.addSubview(userImageView)
        
        
    }
    
    func openAddClassPanel() {
        print("touched")
        
        let navController = AddClassNavController(rootViewController: AddClassMainViewController())
        
        self.presentViewController(navController, animated: true, completion: nil)
    }
    
    //MARK: TableView methods.
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 6
        case 1:
            return 4
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.optionTableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.section == 0 && indexPath.row == 0 {
            self.openAddClassPanel()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Settings"
        case 1:
            return "App Info"
        default:
            return nil
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = self.optionTableView.dequeueReusableCellWithIdentifier("optionCell")
        
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: "optionCell")
        }
        
        cell?.textLabel?.text = "\(indexPath.section): \(indexPath.row)"
        
        if indexPath.section <= 0 {
            cell?.accessoryType = .DisclosureIndicator
            cell?.userInteractionEnabled = true
            
            if indexPath.row == 0 {
                cell?.textLabel?.text = "Add a Class"
            }
            
        } else {
            cell?.accessoryType = .None
            cell?.userInteractionEnabled = false
            cell?.textLabel?.textColor = .grayColor()
            
            if indexPath.row == 0 {
                cell?.textLabel?.text = "Version \(NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"]!) (\(NSBundle.mainBundle().infoDictionary![kCFBundleVersionKey as String]!))"
            } else if indexPath.row == 1 {
                
            } else if indexPath.row == 2 {
                
            } else if indexPath.row == 3 {
                
            }
            
        }
        
        
        
        return cell!
    }
}