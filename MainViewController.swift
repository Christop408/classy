//
//  MainViewController.swift
//  Classy
//
//  Created by Chris Gilardi on 7/8/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import UIKit
import SwiftHEXColors
import pop

class MainViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let length = 6
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nothingLabel: UILabel!
    @IBOutlet weak var getStartedLabel: UILabel!
    
    let colors : [UIColor] = [UIColor(hexString: "#1abc9c")!, UIColor(hexString: "#2ecc71")!, UIColor(hexString: "#3498db")!, UIColor(hexString: "#9b59b6")!, UIColor(hexString: "#34495e")!]
    let classNames : [String] = ["CS101", "CS102", "MATH101", "ENG326", "SCI103"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.hidden = false
        nothingLabel.hidden = true
        getStartedLabel.hidden = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .None
        self.tableView.showsVerticalScrollIndicator = false
        
        
        
        if length == 0 {
            tableView.hidden = true
            nothingLabel.hidden = false
            getStartedLabel.hidden = false
        }
        
        self.view.backgroundColor = Colors.lightGray
        self.tableView.backgroundColor = self.view.backgroundColor
        
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor(hexString: "#ffffff", alpha: 1)
        refreshControl.addTarget(self, action: #selector(MainViewController.refreshTV(_:)), forControlEvents: .ValueChanged)
        
        self.tableView.insertSubview(refreshControl, atIndex: 0)
        
        configureNavbar()
    }
    
    func configureNavbar() {
        
        let userButtonImage = UIImage(named: "maleUser")
        
        
        let button = UIButton(type: .Custom)
        button.setImage(userButtonImage, forState: .Normal)
        button.frame = CGRectMake(0, 0, 30, 30)
        button.backgroundColor = Colors.buttonOrange
        //button.layer.shadowRadius = 10
       // button.layer.shadowColor  = Colors.black.CGColor
       // button.layer.shadowOpacity = 1
        button.layer.cornerRadius = button.frame.size.width/2
        button.addTarget(self, action: #selector(MainViewController.openUserPanel), forControlEvents: .TouchUpInside)
        button.showsTouchWhenHighlighted = true
        
        
        let userButton = UIBarButtonItem(customView: button)
        userButton.tintColor = .whiteColor()
        
        
        self.navigationItem.leftBarButtonItem = userButton
        
        //let addClassButton = AddClassButton(frame: CGRectMake(0, 0, 60, 30), text: "ADD CLASS")
        //addClassButton.setOutlineColor(.whiteColor())
        //addClassButton.addTarget(self, action: #selector(MainViewController.openAddClassPanel(_:)), forControlEvents: .TouchUpInside)
        
        //let addButton = UIBarButtonItem(customView: addClassButton)
        
        //self.navigationItem.rightBarButtonItem = addButton
        
    }
    
    func openUserPanel() {
        print("opening user panel ;)")
        self.presentViewController(UserInfoPanelViewController(), animated: true, completion: nil)
    }
    
    func openAddClassPanel(sender: UIButton) {
        print("touched")
        
        let navController = AddClassNavController(rootViewController: AddClassMainViewController())
        
        self.presentViewController(navController, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return length + 1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0,0,self.view.frame.width, 20))
        headerView.backgroundColor = Colors.lightPrimary
        
        let label = UILabel(frame: CGRectMake(0, 0, 200, 20))
        label.center = CGPointMake(headerView.frame.width/2, headerView.frame.height/2 + 5)
        label.textAlignment = .Center
        label.text = "FALL SEMESTER"
        label.textColor = Colors.white
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Fall Semester"
        }
        else {return "Unknown"}
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = self.tableView.dequeueReusableCellWithIdentifier("cell")
        
        if indexPath.row == length {
            if cell == nil {
                cell = UITableViewCell(style: .Default, reuseIdentifier: "addCell")
                cell?.backgroundColor = .clearColor()
                
                let button = AddClassButton(frame: CGRectMake(self.view.frame.width/2 - 45, 25, 90, 30), text: "ADD CLASS")
                button.setOutlineColor(.whiteColor())
                
                cell?.selectionStyle = .None
                cell?.contentView.addSubview(button)
            }
        } else {
            if cell == nil {
                cell = ClassCell(reuseIdentifier: "cell")
            }
            
            (cell as! ClassCell).callsignLabel?.text = classNames[(indexPath.row + 1) % 5]
            //let color = UIColor.whiteColor()//colors[(indexPath.row + 1) % 5]
            
            
            if indexPath.row % 2 == 0 {
                (cell as! ClassCell).setHoodColor(Colors.darkPrimary)
                cell!.backgroundColor = Colors.darkGray
            } else {
                (cell as! ClassCell).setHoodColor(Colors.lightPrimary)
                cell!.backgroundColor = Colors.lightGray
            }
        }
        

        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let classInfo = ClassInfo(callsign: (self.tableView.cellForRowAtIndexPath(indexPath) as! ClassCell).callsignLabel!.text, name: "Computer Science 102" , description: nil, uid: "8793rhjnklfjds", metadata: nil)
        self.navigationController?.pushViewController(ClassViewController(classInfo: classInfo), animated: true)
    }
    
    func refreshTV(sender: UIRefreshControl) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * Int64(NSEC_PER_SEC)), dispatch_get_main_queue(), {
            sender.endRefreshing()
        })
    }
    
}