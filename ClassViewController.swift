//
//  File.swift
//  Classy
//
//  Created by Chris Gilardi on 7/17/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import UIKit
import GaugeView
import Async
import Eureka


class ClassViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var classInfo : ClassInfo!
    var menuTableView : UITableView!
    var classNameLabel : UILabel!
    var classIconView : UIImageView!
    var classImageView : UIImageView!
    
    convenience init(classInfo : ClassInfo!) {
        self.init()
        self.classInfo = classInfo
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = classInfo.callsign
        self.configureNavbar()
        
        self.view.backgroundColor = Colors.darkGray
        self.view.clipsToBounds = true
        
        self.addClassImage()
        
        self.menuTableView = UITableView(frame: CGRectMake(0, classImageView.height(), self.viewWidth(), self.viewHeight() - self.classImageView.height()), style: .Plain)
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        menuTableView.backgroundColor = .clearColor()
        menuTableView.separatorStyle = .None
        
        self.view.addSubview(menuTableView)
    }
    
    func configureNavbar() {
        let settingsButton = UIBarButtonItem(image: UIImage(named: "settingsButton"), landscapeImagePhone: nil, style: .Plain, target: self, action: #selector(ClassViewController.showSettingsPane))
        self.navigationItem.rightBarButtonItem = settingsButton
    }
    
    func showSettingsPane() {
        let settingsPane = UIAlertController(title: "Class Settings", message: nil, preferredStyle: .ActionSheet)
        settingsPane.addAction(UIAlertAction(title: "Drop Class", style: .Destructive, handler: {action -> Void in
            let confirmationAlert = UIAlertController(title: "Are You Sure?", message: "Are you sure you want to drop \(self.classInfo.callsign) from your enrolled classes?", preferredStyle: .Alert)
            confirmationAlert.addAction(UIAlertAction(title: "Yes", style: .Destructive, handler: {action -> Void in
                print("Dropping Class: \(self.classInfo.callsign)")
            }))
            confirmationAlert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: {action -> Void in
               confirmationAlert.dismissSelf()
            }))
            self.presentViewController(confirmationAlert, animated: true, completion: nil)
        }))
        settingsPane.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: {action -> Void in}))
        
        self.presentViewController(settingsPane, animated: true, completion: nil)
    }
    
    func addClassImage() {
        //TODO: Decide which image based on Metadata -- Personalized images?
        classImageView = UIImageView(image: UIImage(named: "compsci-bg"))
        classImageView.frame = CGRectMake(0, 0, self.view.width(), 200)
        classImageView.contentMode = .ScaleAspectFill
        
        classImageView.layer.shadowColor = UIColor.blackColor().CGColor
        classImageView.layer.shadowRadius = 7
        classImageView.layer.shadowOpacity = 0.5
        
        self.view.addSubview(classImageView)
        
        classIconView = UIImageView(image: UIImage(named: "computer"))
        classIconView.frame = CGRectMake(0, 0, 120, 120)
        classIconView.center = CGPointMake(classImageView.width()/2, classImageView.height()/2 - 25)
        classIconView.layer.shadowColor = UIColor.blackColor().CGColor
        classIconView.layer.shadowRadius = 7
        classIconView.layer.shadowOpacity = 0.5
        classImageView.addSubview(classIconView)
        
        classNameLabel = UILabel(frame: CGRectMake(0,0, 200, 30))
        classNameLabel.text = self.classInfo.name
        classNameLabel.textColor = Colors.white
        classNameLabel.font = UIFont.boldSystemFontOfSize(18)
        classNameLabel.textAlignment = .Center
        classNameLabel.center = classIconView.center.translatedBy(0, y: 90)
        classNameLabel.adjustsFontSizeToFitWidth = true
        
        classNameLabel.backgroundColor = Colors.darkGray
        classNameLabel.layer.cornerRadius = 10
        classNameLabel.clipsToBounds = true
        classNameLabel.layer.shadowColor = UIColor.blackColor().CGColor
        classNameLabel.layer.shadowRadius = 7
        classNameLabel.layer.shadowOpacity = 0.5
        
        classImageView.addSubview(classNameLabel)
        
    }
    
    //MARK: TableView Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        switch indexPath.row {
        case 1:
            self.navigationController?.pushViewController(ClassPostsViewController(classInfo: self.classInfo), animated: true)
        default:
            break
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("test")
        
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: "test")
        }
        
        cell?.accessoryType = .DisclosureIndicator
        
        if indexPath.row == 0 {
            cell?.textLabel?.text = "Class Planner"
        } else if indexPath.row == 1 {
            cell?.textLabel?.text = "Message Board"
        } else if indexPath.row == 2 {
            cell?.textLabel?.text = "Class Information"
        }
        
        cell?.textLabel?.textColor = Colors.white
        
        if indexPath.row % 2 == 0 {
            cell?.backgroundColor = Colors.lightGray
        } else {
            cell?.backgroundColor = Colors.darkGray
        }
        
        return cell!
    }
    
}

class ClassPostsViewController : UITableViewController {
    
    var classInfo : ClassInfo!
    var postButton : UIView!
    
    convenience init(classInfo: ClassInfo!) {
        self.init(style: .Plain)
        self.classInfo = classInfo

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Discussion"
        
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.separatorStyle = .None
        self.view.backgroundColor = Colors.offWhite
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0,0,self.view.width(),60))
        headerView.backgroundColor = Colors.offWhite
        
        let addPostButton = AddClassButton(frame: CGRectMake(10, 10, self.viewWidth() - 20, headerView.height() - 20), text: "Post")
        addPostButton.label.textColor = Colors.white
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ClassPostsViewController.postButtonTapped))
        addPostButton.addGestureRecognizer(tapRecognizer)
        headerView.addSubview(addPostButton)
        
        return headerView
 
    }
    
    func postButtonTapped() {
        print("tapped")
        self.presentViewController(PostNavController(classInfo: self.classInfo), animated: true, completion: nil)
    }

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 300
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCellWithIdentifier(kPostCellIdentifier) as? PostCell
        
        let user = UserInfo(name: "Chris Gilardi", uid: "ab2334098x8jkl", friends: false, schoolInfo: nil)
        
        if cell == nil {
            cell = PostCell(post: Post(user: user, uid: "ajsdk23jkj340vjzx", body: kLorem, date: NSDate()), navController: self.navigationController)
        }
        
        
        cell?.selectionStyle = .None
        return cell!
    }
}

class PostNavController : UINavigationController {
    
    convenience init(classInfo : ClassInfo!) {
        self.init(rootViewController : PostCreateController(classInfo: classInfo))
        self.setupNavbar()
    }
    
    func setupNavbar() {
        self.navigationBar.tintColor = Colors.white
        self.navigationBar.barTintColor = Colors.darkGray
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : Colors.white]
    }
}

class PostCreateController : FormViewController {
    
    convenience init(classInfo : ClassInfo!) {
        self.init(style: .Grouped)
        self.title = "New Post"
        self.setupNavbar()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        form +++ Section("Title")
            <<< TextRow() { row in
                row.tag = "titleRow"
                row.placeholder = "A short, catchy title."
            }
        form +++ Section("Post Body")
            <<< TextAreaRow() { row in
                row.tag = "descriptionRow"
                row.placeholder = "Post Body Goes Here"
            }
            <<< ButtonRow() { row in
                row.title = "Post"
                row.cell.textLabel?.textColor = Colors.buttonOrange
                row.onCellSelection({cell, row in
                    self.post()
                })
            }
    }
    
    func setupNavbar() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(UIViewController.dismissSelf))
    }
    
    func post() {
        //TODO: Implement Posting
        
        let loadingView = LoadingView(frame: CGRectMake(0,0,self.viewWidth()/2, self.viewWidth()/2))
        loadingView.center = self.view.center
        loadingView.startAnimating()
        
        self.view.addSubview(loadingView)
        
        Async.main(after: 2, block: {
            self.dismissSelf()
            loadingView.stopAnimating()
        })
    }
}

class PostCell : UITableViewCell {
    
    var userNameLabel : UILabel!
    var userPicLabel  : UIImageView!
    var postDateLabel : UILabel!
    var postBodyLabel : UILabel!
    var postBodyView : UIView!
    
    var opensPost : Bool = true
    
    var post : Post!
    
    var navController : UINavigationController!
    
    convenience init(post : Post, navController : UINavigationController!) {
        self.init(style: UITableViewCellStyle.Default, reuseIdentifier: kPostCellIdentifier)
        
        self.post = post
        self.navController = navController
        
        let userView = UIView(frame: CGRectMake(0,0, self.width()*2,60))
        
        userView.backgroundColor = Colors.offWhite
        
        userPicLabel = UIImageView(image: UIImage(named: "cgilardi"))
        userPicLabel.frame = CGRectMake(15, 10, 40, 40)
        userPicLabel.layer.cornerRadius = userPicLabel.width()/2
        userPicLabel.clipsToBounds = true
        userPicLabel.layer.borderWidth = 2
        userPicLabel.layer.borderColor = Colors.buttonOrange.CGColor
        
        userNameLabel = UILabel(frame: CGRectMake(userPicLabel.frame.width + 25, userPicLabel.frame.origin.y, 100, 20))
        userNameLabel.text = post.user.name
        userNameLabel.textColor = Colors.blue
        
        postDateLabel = UILabel(frame: CGRectMake(userNameLabel.frame.origin.x, userNameLabel.frame.origin.y + userNameLabel.height(), 150, 20))
        postDateLabel.textColor = .grayColor()
        postDateLabel.adjustsFontSizeToFitWidth = false
        postDateLabel.text = post.date.timeAgo()
        
        self.addSubview(userView)
        userView.addSubview(userPicLabel)
        userView.addSubview(userNameLabel)
        userView.addSubview(postDateLabel)
        userView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PostCell.openUserPage)))
        
        //TODO: Add a Post Body View to house possible pictures, other UI elements.
        
        postBodyView = UIView(frame: CGRectMake(0, userView.height(), self.width(), 240))
        postBodyLabel = UILabel(frame: CGRectMake(5,5,self.postBodyView.width() - 10, self.postBodyView.height() - 10))
        postBodyLabel.adjustsFontSizeToFitWidth = false
        postBodyLabel.font = UIFont.systemFontOfSize(16)
        postBodyLabel.lineBreakMode = .ByWordWrapping
        postBodyLabel.text = post.body
        postBodyLabel.numberOfLines = 0
        postBodyView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PostCell.openPostVC)))
        if debug {
            self.postBodyView.backgroundColor = .redColor()
            self.postBodyLabel.backgroundColor = .blueColor()
        }
        self.addSubview(postBodyView)
        self.postBodyView.addSubview(postBodyLabel)
        print(userView.height())
    }
    
    func openUserPage() {
        self.navController.pushViewController(UserViewController(userInfo: post.user), animated: true)
    }
    
    func openPostVC() {
        if opensPost {
            self.navController.pushViewController(PostViewController(post: self.post, navController: self.navController), animated: true)
        }
    }
    
}


