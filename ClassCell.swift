//
//  ClassCell.swift
//  Classy
//
//  Created by Chris Gilardi on 7/8/16.
//  Copyright © 2016 Chris Gilardi. All rights reserved.
//

import Foundation
import UIKit
import pop
import Async

public class ClassCell : UITableViewCell {
    
    let split = (3/16) as CGFloat
    public var callsignLabel : UILabel?
    private let hoodLayer = CAShapeLayer()
    
    let showTopLine = false
    
    init(reuseIdentifier ri: String?) {
        super.init(style: .Default, reuseIdentifier: ri)
        let path = UIBezierPath(rect: CGRectMake(0, 0, self.frame.width*split, self.bounds.height*2 - 7.5))
        path.addLineToPoint(CGPointMake(self.frame.width*(split*2), 0))
        if showTopLine {
            path.addLineToPoint(CGPointMake(self.frame.width*1.5, 0))
            path.addLineToPoint(CGPointMake(self.frame.width*1.5, self.frame.height/2))
        }
        path.addLineToPoint(CGPointMake(self.frame.width*(split*2), 0))
        path.addLineToPoint(CGPointMake(self.frame.width*split, self.bounds.height*2 - 5))
        
        hoodLayer.path = path.CGPath
        hoodLayer.fillColor = UIColor.blackColor().CGColor
        
        self.layer.insertSublayer(hoodLayer, atIndex: 0)
        
        self.accessoryType = .DisclosureIndicator
        
        callsignLabel = UILabel(frame: CGRectMake(10, self.frame.height/2, self.frame.width*split, self.frame.height))
        callsignLabel?.text = "TST"
        callsignLabel?.textColor = .whiteColor()
        callsignLabel?.adjustsFontSizeToFitWidth = true
        callsignLabel?.minimumScaleFactor = 0.2
        callsignLabel?.baselineAdjustment = .AlignCenters
        callsignLabel?.font = callsignLabel!.font.fontWithSize(60)
        
        self.addSubview(callsignLabel!)
        
        self.backgroundColor = UIColor(hexString: "#7f8c8d")
        
        self.clipsToBounds = true
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setHoodColor(color: UIColor) {
        self.hoodLayer.fillColor = color.CGColor
    }
    
}


class AddClassButton : UIButton {
    
    var label : UILabel!
    
    init(frame: CGRect, text: String) {
        super.init(frame: frame)
        self.backgroundColor = .clearColor()
        //self.layer.borderColor = UIColor(hexString: "#42A5F5", alpha: 1)?.CGColor
        //self.layer.borderWidth = 1
        self.layer.cornerRadius = 3
        self.layer.backgroundColor = UIColor(hexString: "#FF5722")?.CGColor //UIColor.clearColor().CGColor
        
        //self.layer.shadowRadius = 10
        //self.layer.shadowColor = UIColor.blackColor().CGColor
        //self.layer.shadowOpacity = 1
        
        label = UILabel(frame: CGRectMake(5, 5, self.frame.width - 10, self.frame.height - 10))
        label.textAlignment = .Center
        label.baselineAdjustment = .AlignCenters
        label.text = text
        label.textColor = UIColor(hexString: "#42A5F5", alpha: 1)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.2
        label.font = UIFont.boldSystemFontOfSize(20.0)
        self.addSubview(label)
        
        setupAnimations()
        
        self.addTarget(self, action: #selector(AddClassButton.touched), forControlEvents: .TouchUpInside)
        
    }
    
    func setupAnimations() {
        //TODO: Implement
    }
    
    func touched() {
        let animation = POPSpringAnimation(propertyNamed: kPOPLayerShadowRadius)
        animation.fromValue = 10
        animation.toValue = 2
        
        let backAnimation = POPSpringAnimation(propertyNamed: kPOPLayerShadowRadius)
        backAnimation.fromValue = 2
        backAnimation.toValue = 10
        
        self.layer.pop_addAnimation(animation, forKey: "touch")
        
        Async.main(after: (1/3), block: {
            self.layer.pop_addAnimation(backAnimation, forKey: "touchup")
        })
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setOutlineColor(color: UIColor) {
        label.textColor = color
        self.layer.borderColor = color.CGColor
    }
    
}




